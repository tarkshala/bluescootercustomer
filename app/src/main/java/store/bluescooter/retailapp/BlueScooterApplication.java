/*
 * Copyright (c) 2018. http://hiteshsahu.com- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * If you use or distribute this project then you MUST ADD A COPY OF LICENCE
 * along with the project.
 *  Written by Hitesh Sahu <hiteshkrsahu@Gmail.com>, 2017.
 */

package store.bluescooter.retailapp;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.google.gson.Gson;
import store.bluescooter.retailapp.domain.network.VolleyRequestQueueProvider;
import store.bluescooter.retailapp.util.PreferenceHelper;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import lombok.Getter;
import lombok.Setter;

//@ReportsCrashes(mailTo = "kuldeep.writes@gmail.com", customReportContent = {
//        ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME,
//        ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL,
//        ReportField.CUSTOM_DATA, ReportField.STACK_TRACE, ReportField.LOGCAT},
//        mode = ReportingInteractionMode.TOAST, resToastText = store.bluescooter.retailapp.R.string.crash_toast_text)
public class BlueScooterApplication extends Application implements VolleyRequestQueueProvider{

    public static final String TAG = BlueScooterApplication.class.getSimpleName();

    private static BlueScooterApplication mInstance;

    public static synchronized BlueScooterApplication getInstance() {
        return mInstance;
    }

    private static RequestQueue networkRequestQueue;

    private static Gson gson;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        // The following line triggers the initialization of ACRA for crash Log Reporting
        if (PreferenceHelper.getPrefernceHelperInstace().getBoolean(
                this, PreferenceHelper.SUBMIT_LOGS, true)) {
            ACRA.init(this);
        }
        initNetworkRequestQueue();
        initGson();
    }

    @Override
    public RequestQueue getNetworkQueue() {
        return this.networkRequestQueue;
    }

    private void initNetworkRequestQueue() {
        // Instantiate the cache with a cap of 10 MB
        Cache cache = new DiskBasedCache(getCacheDir(), 10 * 1024 * 1024);
        // Set up the network to use HttpURLConnection as the HTTP client.
        Network network = new BasicNetwork(new HurlStack());
        // Instantiate the RequestQueue with the cache and network.
        this.networkRequestQueue = new RequestQueue(cache, network);
        this.networkRequestQueue.start();
    }

    public Gson getGson() {
        return gson;
    }

    private void initGson() {
        this.gson = new Gson();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }
}
