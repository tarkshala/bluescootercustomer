package store.bluescooter.retailapp.view.fragment;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import store.bluescooter.retailapp.R;
import store.bluescooter.retailapp.db.room.database.BSDatabase;
import store.bluescooter.retailapp.db.room.enitity.Address;
import store.bluescooter.retailapp.model.CenterRepository;
import store.bluescooter.retailapp.view.activities.ECartHomeActivity;

import static store.bluescooter.retailapp.BlueScooterApplication.TAG;

public class AddressFragment extends Fragment {

    private CollapsingToolbarLayout collapsingToolbar;
    int mutedColor = R.attr.colorPrimary;

    private List<Address> addresses;
    private BSDatabase database;
    private View rootView;
    public static final String NAME = "AddressFragment";

    public static Fragment newInstance() {
        return new AddressFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        rootView = inflater.inflate(R.layout.frag_address, container, false);
        final Toolbar toolbar = rootView.findViewById(R.id.anim_toolbar);
        ((ECartHomeActivity) getActivity()).setSupportActionBar(toolbar);
        ((ECartHomeActivity) getActivity()).getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_drawer);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ECartHomeActivity) getActivity()).getmDrawerLayout()
                        .openDrawer(GravityCompat.START);
            }
        });

        collapsingToolbar = rootView.findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle("Address");

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.header);

        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
            @SuppressWarnings("ResourceType")
            @Override
            public void onGenerated(Palette palette) {

                mutedColor = palette.getMutedColor(R.color.primary_500);
                collapsingToolbar.setContentScrimColor(mutedColor);
                collapsingToolbar.setStatusBarScrimColor(R.color.black_trans80);
            }
        });

        return rootView;
    }

    private void loadFormWithAddress(Address address) {
        ((TextView)rootView.findViewById(R.id.name_address)).setText(address.getName());
        ((TextView)rootView.findViewById(R.id.phone_address)).setText(address.getPhone());
        ((TextView)rootView.findViewById(R.id.address)).setText(address.getAddress());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        database = Room.databaseBuilder(getActivity(), BSDatabase.class, BSDatabase.DB_NAME).build();
        loadAddresses();
    }

    @Override
    public void onStart() {
        super.onStart();

        // TODO: 25/10/18 : https://bitbucket.org/tarkshala/bluescootercustomer/issues/1/login-with-google-on-the-address-page 
    }

    @Override
    public void onPause() {
        super.onPause();
        if (!TextUtils.isEmpty(((EditText)rootView.findViewById(R.id.phone_address)).getText())) {
            Address address = new Address();
            address.setName(((EditText)rootView.findViewById(R.id.name_address)).getText().toString());
            address.setPhone(((EditText)rootView.findViewById(R.id.phone_address)).getText().toString());
            address.setAddress(((EditText)rootView.findViewById(R.id.address)).getText().toString());
            if (!addresses.isEmpty()) {
                address.setUid(1L);
                updateAddress(address);
            } else {
                saveAddress(address);
            }
            CenterRepository.getCenterRepository().setSelectedAddress(address);
        }
    }

    private void loadAddresses() {
        new AsyncTask<Void, Void, List<Address>>() {
            @Override
            protected List<Address> doInBackground(Void... voids) {
                return database.getAddressDao().getAll();
            }

            @Override
            protected void onPostExecute(List<Address> addressList) {
                addresses = addressList;
                if (addresses != null && !addresses.isEmpty()) {
                    loadFormWithAddress(addresses.get(0));
                }
                Log.i(TAG, "onPostExecute: " + new Gson().toJson(addresses));
            }
        }.execute();
    }

    private void saveAddress(Address address) {
        new AsyncTask<Address, Void, Void>() {
            @Override
            protected Void doInBackground(Address... addresses) {
                database.getAddressDao().insert(addresses);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                loadAddresses();
            }
        }.execute(address);
    }

    private void updateAddress(Address address) {
        new AsyncTask<Address, Void, Void>() {
            @Override
            protected Void doInBackground(Address... addresses) {
                database.getAddressDao().update(addresses);
                return null;
            };

            @Override
            protected void onPostExecute(Void aVoid) {
                loadAddresses();
            }
        }.execute(address);
    }

    public boolean validateAddress() {
        if (TextUtils.isEmpty(((TextView) rootView.findViewById(R.id.name_address)).getText()) ||
                TextUtils.isEmpty(((TextView)rootView.findViewById(R.id.phone_address)).getText()) ||
                TextUtils.isEmpty(((TextView)rootView.findViewById(R.id.address)).getText())) {
            return false;
        }
        return true;
    }
}
