package store.bluescooter.retailapp.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import store.bluescooter.retailapp.R;
import store.bluescooter.retailapp.view.activities.ECartHomeActivity;

public class NoInternetFragment extends Fragment {

    public static final String NAME = "NoInternetFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_no_internet, container, false);
        Button button = view.findViewById(R.id.no_internet_retry);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ECartHomeActivity) getActivity()).restartApp();
            }
        });
        return view;
    }
}
