package store.bluescooter.retailapp.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import store.bluescooter.retailapp.R;
import store.bluescooter.retailapp.util.Utils;

public class OrderSummaryFragment extends Fragment {

    public static final String NAME = "OrderSummaryFragment";

    public static Fragment newInstance() {
        return new OrderSummaryFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.frag_summary, container, false);

        TextView message = rootView.findViewById(R.id.summary_message);
        message.append("We will try to deliver your order with in 1 hour.");

        rootView.findViewById(R.id.summary_shop_more_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.switchFragmentWithAnimation(R.id.frag_container, new HomeFragment(), getActivity(), Utils.HOME_FRAGMENT, Utils.AnimationType.SLIDE_UP);
            }
        });
        return rootView;
    }
}
