/*
 * Copyright (c) 2017. http://hiteshsahu.com- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * If you use or distribute this project then you MUST ADD A COPY OF LICENCE
 * along with the project.
 *  Written by Hitesh Sahu <hiteshkrsahu@Gmail.com>, 2017.
 */

package store.bluescooter.retailapp.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.tarkshala.dukan.business.model.CreateOrderRequest;
import com.tarkshala.dukan.business.model.bo.AddressBO;
import com.tarkshala.dukan.business.model.bo.OrderBO;
import com.tarkshala.dukan.business.model.bo.ProductBO;
import com.tarkshala.volley.dukan.SubmitRequest;
import com.wang.avi.AVLoadingIndicatorView;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import store.bluescooter.retailapp.BlueScooterApplication;
import store.bluescooter.retailapp.R;
import store.bluescooter.retailapp.db.room.enitity.Address;
import store.bluescooter.retailapp.domain.helper.Connectivity;
import store.bluescooter.retailapp.domain.mining.AprioriFrequentItemsetGenerator;
import store.bluescooter.retailapp.model.CenterRepository;
import store.bluescooter.retailapp.model.entities.Money;
import store.bluescooter.retailapp.model.entities.Product;
import store.bluescooter.retailapp.util.AppConstants;
import store.bluescooter.retailapp.util.PreferenceHelper;
import store.bluescooter.retailapp.util.TinyDB;
import store.bluescooter.retailapp.util.Utils;
import store.bluescooter.retailapp.util.Utils.AnimationType;
import store.bluescooter.retailapp.view.fragment.AddressFragment;
import store.bluescooter.retailapp.view.fragment.HomeFragment;
import store.bluescooter.retailapp.view.fragment.NoInternetFragment;
import store.bluescooter.retailapp.view.fragment.PaymentFragment;
import store.bluescooter.retailapp.view.fragment.WhatsNewDialog;

import static com.android.volley.DefaultRetryPolicy.DEFAULT_BACKOFF_MULT;
import static com.android.volley.DefaultRetryPolicy.DEFAULT_MAX_RETRIES;
import static store.bluescooter.retailapp.util.AppConstants.TIMEOUT_MS;

public class ECartHomeActivity extends AppCompatActivity {

    private static final String TAG = ECartHomeActivity.class.getSimpleName();
    AprioriFrequentItemsetGenerator<String> generator =
            new AprioriFrequentItemsetGenerator<>();
    private int itemCount = 0;
    private BigDecimal checkoutAmount = new BigDecimal(BigInteger.ZERO);
    private DrawerLayout mDrawerLayout;

    private TextView checkOutAmount, itemCountTextView;
    private TextView offerBanner;
    private AVLoadingIndicatorView progressBar;

    private NavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecart);

//        CenterRepository.getCenterRepository().setListOfProductsInShoppingList(
//                new TinyDB(getApplicationContext()).getListObject(
//                        PreferenceHelper.MY_CART_LIST_LOCAL, Product.class));

        CenterRepository.getCenterRepository().setListOfProductsInShoppingList(new ArrayList<Product>());

        itemCount = CenterRepository.getCenterRepository().getListOfProductsInShoppingList().size();

        offerBanner = ((TextView) findViewById(R.id.new_offers_banner));

        itemCountTextView = (TextView) findViewById(R.id.item_count);
        itemCountTextView.setSelected(true);
        itemCountTextView.setText(String.valueOf(itemCount));

        checkOutAmount = (TextView) findViewById(R.id.checkout_amount);
        checkOutAmount.setSelected(true);
        checkOutAmount.setText(Money.rupees(checkoutAmount).toString());
        offerBanner.setSelected(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.nav_drawer);
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);

        progressBar = (AVLoadingIndicatorView) findViewById(R.id.loading_bar);

        checkOutAmount.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Utils.vibrate(getApplicationContext());

                Utils.switchContent(R.id.frag_container,
                        Utils.SHOPPING_LIST_TAG, ECartHomeActivity.this,
                        AnimationType.SLIDE_UP);

            }
        });


        if (itemCount != 0) {
            for (Product product : CenterRepository.getCenterRepository()
                    .getListOfProductsInShoppingList()) {

                updateCheckOutAmount(
                        BigDecimal.valueOf(Long.valueOf(product.getSellMRP())),
                        true);
            }
        }

        findViewById(R.id.item_counter).setOnClickListener(
                new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        Utils.vibrate(getApplicationContext());
                        Utils.switchContent(R.id.frag_container,
                                Utils.SHOPPING_LIST_TAG,
                                ECartHomeActivity.this, AnimationType.SLIDE_UP);

                    }
                });

        findViewById(R.id.checkout).setOnClickListener(
            new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO: 19/10/18 Start implementing billing from this point
                    Fragment existingFragment = getSupportFragmentManager().findFragmentById(R.id.frag_container);

                    if ((existingFragment != null && existingFragment.getTag() != null &&
                            existingFragment.getTag().equals(Utils.HOME_FRAGMENT)) ||
                            (existingFragment != null && existingFragment.getTag() !=null &&
                            existingFragment.getTag().equals(Utils.PRODUCT_OVERVIEW_FRAGMENT_TAG))) {
                        Utils.switchFragmentWithAnimation(R.id.frag_container, AddressFragment.newInstance(),
                                ECartHomeActivity.this, AddressFragment.NAME, AnimationType.SLIDE_DOWN);
                    } else if (existingFragment != null && existingFragment.getTag() != null &&
                            existingFragment.getTag().equals(AddressFragment.NAME)) {

                        if(!((AddressFragment)existingFragment).validateAddress()) {
                            // open error dialog
                            Log.i(TAG, "onClick: Invalid address form");
                            Toast.makeText(ECartHomeActivity.this, "Please give valid address details", Toast.LENGTH_LONG).show();
                        } else {
                            Utils.switchFragmentWithAnimation(R.id.frag_container, PaymentFragment.newInstance(),
                                    ECartHomeActivity.this, PaymentFragment.NAME, AnimationType.SLIDE_DOWN);
                        }
                    } else if (existingFragment != null && existingFragment.getTag() != null &&
                            existingFragment.getTag().equals(PaymentFragment.NAME)) {
                        placeOrder();

                        Utils.switchFragmentWithAnimation(R.id.frag_container,
                                new HomeFragment(), ECartHomeActivity.this, Utils.HOME_FRAGMENT,
                                AnimationType.SLIDE_UP);

                        toggleBannerVisibility();
                    }
                }
            });

        mNavigationView
            .setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(MenuItem menuItem) {

                    menuItem.setChecked(true);
                    switch (menuItem.getItemId()) {
                        case R.id.home:

                            mDrawerLayout.closeDrawers();

                            Utils.switchContent(R.id.frag_container,
                                    Utils.HOME_FRAGMENT,
                                    ECartHomeActivity.this,
                                    AnimationType.SLIDE_LEFT);

                            return true;

                        case R.id.my_cart:

                            mDrawerLayout.closeDrawers();

                            Utils.switchContent(R.id.frag_container,
                                    Utils.SHOPPING_LIST_TAG,
                                    ECartHomeActivity.this,
                                    AnimationType.SLIDE_LEFT);
                            return true;

                        case R.id.apriori_result:

                            mDrawerLayout.closeDrawers();

                            startActivity(new Intent(ECartHomeActivity.this, APrioriResultActivity.class));

                            return true;


                        case R.id.contact_us:

                            mDrawerLayout.closeDrawers();

                            Utils.switchContent(R.id.frag_container,
                                    Utils.CONTACT_US_FRAGMENT,
                                    ECartHomeActivity.this,
                                    AnimationType.SLIDE_LEFT);
                            return true;

                        case R.id.settings:

                            mDrawerLayout.closeDrawers();

                            Utils.switchContent(R.id.frag_container,
                                    Utils.SETTINGS_FRAGMENT_TAG,
                                    ECartHomeActivity.this,
                                    AnimationType.SLIDE_LEFT);
                            return true;
                        default:
                            return true;
                    }
                }
            });

//        if (!((BlueScooterApplication)getApplicationContext()).isNetworkAvailable()) {
//            Utils.switchFragmentWithAnimation(R.id.frag_container,
//                    new NoInternetFragment(), ECartHomeActivity.this, NoInternetFragment.NAME,
//                    AnimationType.SLIDE_DOWN);
//        } else {
//            Utils.switchFragmentWithAnimation(R.id.frag_container,
//                    new HomeFragment(), ECartHomeActivity.this, Utils.HOME_FRAGMENT,
//                    AnimationType.SLIDE_UP);
//        }
        Utils.switchFragmentWithAnimation(R.id.frag_container,
                new HomeFragment(), ECartHomeActivity.this, Utils.HOME_FRAGMENT,
                AnimationType.SLIDE_UP);
        toggleBannerVisibility();
    }

    public AVLoadingIndicatorView getProgressBar() {
        return progressBar;
    }

    private void placeOrder() {

        getProgressBar().setVisibility(View.VISIBLE);
        List<ProductBO> products = convertProducts(CenterRepository.getCenterRepository().getListOfProductsInShoppingList());
        Address address = CenterRepository.getCenterRepository().getSelectedAddress();
        OrderBO order = OrderBO.builder()
                .buyerName(address.getName())
                .date(new Date().getTime())
                .products(products)
                .address(convertAddress(address))
                .build();

        CreateOrderRequest createOrderRequest = CreateOrderRequest.builder().order(order).build();
        SubmitRequest<CreateOrderRequest> submitRequest = new SubmitRequest<>(
                AppConstants.CREATE_ORDER_URL, createOrderRequest,
                new OrderRequestListener() ,new OrderRequestErrorListener());
        // TODO: 11/11/18 Change timeout
        submitRequest.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, DEFAULT_MAX_RETRIES, DEFAULT_BACKOFF_MULT));
        BlueScooterApplication.getInstance().getNetworkQueue().add(submitRequest);
    }

    private class OrderRequestListener implements Response.Listener {

        @Override
        public void onResponse(Object response) {
            Log.i(TAG, response.toString());
            Toast.makeText(ECartHomeActivity.this, "Order have been placed successfully", Toast.LENGTH_LONG).show();
            getProgressBar().setVisibility(View.GONE);

            CenterRepository.getCenterRepository().getListOfProductsInShoppingList().clear();

            resetCart();
            toggleBannerVisibility();
        }
    }

    private class OrderRequestErrorListener implements Response.ErrorListener {

        @Override
        public void onErrorResponse(VolleyError error) {
            Log.e(TAG, "onErrorResponse: ", error);
            getProgressBar().setVisibility(View.GONE);
            Toast.makeText(ECartHomeActivity.this,"Sorry, Order creation failed. We are working on this problem.", Toast.LENGTH_LONG).show();
        }
    }

    private List<ProductBO> convertProducts(List<Product> products) {

        List<ProductBO> list = new ArrayList<>();
        for (Product product: products) {
            ProductBO productBO = new ProductBO();
            productBO.setName(product.getItemName());
            productBO.setDescription(product.getItemDetail());
            productBO.setDescription(product.getItemShortDesc());
            productBO.setMrp(Float.parseFloat(product.getMRP()));
            productBO.setDiscount(Float.parseFloat(product.getDiscount()));
            productBO.setUid(product.getUid());
            productBO.setImageUrl(product.getImageURL());
            productBO.setQuantity(Float.parseFloat(product.getQuantity()));
            list.add(productBO);
        }
        return list;
    }

    private AddressBO convertAddress(Address address) {

        return AddressBO
                .builder()
                .name(address.getName())
                .phone(address.getPhone())
                .address(address.getAddress())
                .build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void updateItemCount(boolean ifIncrement) {
        if (ifIncrement) {
            itemCount++;
            itemCountTextView.setText(String.valueOf(itemCount));

        } else {
            itemCountTextView.setText(String.valueOf(itemCount <= 0 ? 0
                    : --itemCount));
        }

        toggleBannerVisibility();
    }

    public void updateCheckOutAmount(BigDecimal amount, boolean increment) {

        if (increment) {
            checkoutAmount = checkoutAmount.add(amount);
        } else {
            if (checkoutAmount.signum() == 1)
                checkoutAmount = checkoutAmount.subtract(amount);
        }

        checkOutAmount.setText(Money.rupees(checkoutAmount).toString());
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Store Shopping Cart in DB
        new TinyDB(getApplicationContext()).putListObject(
                PreferenceHelper.MY_CART_LIST_LOCAL, CenterRepository
                        .getCenterRepository().getListOfProductsInShoppingList());
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Show Offline Error Message
        if (!Connectivity.isConnected(getApplicationContext())) {
            Utils.switchFragmentWithAnimation(R.id.frag_container,
                    new NoInternetFragment(), ECartHomeActivity.this, NoInternetFragment.NAME,
                    AnimationType.SLIDE_DOWN);
        }

        // Show Whats New Features If Requires
        new WhatsNewDialog(this);
    }

    /*
     * Toggles Between Offer Banner and Checkout Amount. If Cart is Empty SHow
     * Banner else display total amount and item count
     */
    public void toggleBannerVisibility() {
        if (itemCount == 0) {

            findViewById(R.id.checkout_item_root).setVisibility(View.GONE);
            findViewById(R.id.new_offers_banner).setVisibility(View.VISIBLE);

        } else {
            findViewById(R.id.checkout_item_root).setVisibility(View.VISIBLE);
            findViewById(R.id.new_offers_banner).setVisibility(View.GONE);
        }
    }

    public void resetCart() {
        checkoutAmount = BigDecimal.ZERO;
        checkOutAmount.setText(null);

        itemCount = 0;
        itemCountTextView.setText(null);
    }

    /*
     * get total checkout amount
     */
    public BigDecimal getCheckoutAmount() {
        return checkoutAmount;
    }

    /*
     * Get Number of items in cart
     */
    public int getItemCount() {
        return itemCount;
    }

    /*
     * Get Navigation drawer
     */
    public DrawerLayout getmDrawerLayout() {
        return mDrawerLayout;
    }

    /**
     * Restart the application
     */
    public void restartApp(){
        Intent intent = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage( getBaseContext().getPackageName() );
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}
