package store.bluescooter.retailapp.db.room.enitity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Address {

    @PrimaryKey(autoGenerate = true)
    private long uid;

    private String name;

    private String phone;

    private String address;
}
