package store.bluescooter.retailapp.db.room.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import store.bluescooter.retailapp.db.room.dao.AddressDao;
import store.bluescooter.retailapp.db.room.enitity.Address;

@Database(entities = {Address.class}, version = 1)
public abstract class BSDatabase extends RoomDatabase {

    public static final String DB_NAME = "blue_scooter_db";

    public abstract AddressDao getAddressDao();
}
