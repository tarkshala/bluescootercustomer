package store.bluescooter.retailapp.db.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import store.bluescooter.retailapp.db.room.enitity.Address;

@Dao
public interface AddressDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Address... address);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Address... addresses);

    @Query("SELECT * FROM address")
    List<Address> getAll();
}
