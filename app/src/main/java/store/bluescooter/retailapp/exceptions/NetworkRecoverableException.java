package store.bluescooter.retailapp.exceptions;

public class NetworkRecoverableException extends RecoverableException {
    public NetworkRecoverableException(String message, Throwable cause) {
        super(message, cause);
    }
}
