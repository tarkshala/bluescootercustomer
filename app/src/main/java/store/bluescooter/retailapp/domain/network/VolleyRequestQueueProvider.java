/*
 * Copyright (c) 2018. http://hiteshsahu.com- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * If you use or distribute this project then you MUST ADD A COPY OF LICENCE
 * along with the project.
 *  Written by Hitesh Sahu <hiteshkrsahu@Gmail.com>, 2017.
 */

package store.bluescooter.retailapp.domain.network;

import com.android.volley.RequestQueue;

public interface VolleyRequestQueueProvider {

    /**
     * Provide a network queue to send the requests to rest APIs
     * @return volley request queue
     */
    RequestQueue getNetworkQueue();
}
