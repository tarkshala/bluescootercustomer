package store.bluescooter.retailapp.domain.api;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.tarkshala.dukan.business.model.FetchProductsRequest;
import com.tarkshala.dukan.business.model.FetchProductsResponse;
import com.tarkshala.dukan.business.model.bo.ProductBO;
import com.tarkshala.dukan.business.model.filter.Filter;
import com.tarkshala.volley.dukan.QueryRequest;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

import store.bluescooter.retailapp.BlueScooterApplication;
import store.bluescooter.retailapp.R;
import store.bluescooter.retailapp.exceptions.RecoverableException;
import store.bluescooter.retailapp.model.CenterRepository;
import store.bluescooter.retailapp.model.entities.Product;
import store.bluescooter.retailapp.util.AppConstants;
import store.bluescooter.retailapp.view.activities.ECartHomeActivity;
import store.bluescooter.retailapp.view.adapter.ProductsInCategoryPagerAdapter;
import store.bluescooter.retailapp.view.fragment.ProductListFragment;

import static store.bluescooter.retailapp.BlueScooterApplication.TAG;

public class ProductLoader {

    private Context context;
    private ViewPager viewPager;
    private TabLayout tabs;

    private static final String PRODUCT_CATEGORY_UID = "category.uid";

    public ProductLoader(Context context, ViewPager viewPager, TabLayout tabs) {
        this.context = context;
        this.viewPager = viewPager;
        this.tabs = tabs;
    }

    public void load(String... params) {

        // do whatever is needed before execution of the task like progress bar
        if (null != ((ECartHomeActivity) context).getProgressBar())
            ((ECartHomeActivity) context).getProgressBar().setVisibility(
                    View.VISIBLE);

        FetchProductsRequest requestBO = composeFetchCategoriesRequest(params[0]);

        QueryRequest<FetchProductsRequest, Type> request = new QueryRequest<>(
                AppConstants.FETCH_PRODUCTS_URL,
                requestBO,
                new ProductsLoaderListener(),
                new ProductsLoaderErrorListener(),
                FetchProductsResponse.class,
                true);
        request.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        BlueScooterApplication.getInstance().getNetworkQueue().add(request);
    }

    private FetchProductsRequest composeFetchCategoriesRequest(String category) {

        Filter filter = Filter.builder().
                attributeName(PRODUCT_CATEGORY_UID).
                attributeType(Filter.AttributeType.STRING).
                operation(Filter.Operation.EQ).
                attributeValue(category).build();
        return new FetchProductsRequest(Collections.singletonList(filter), null, null);
    }

    private void setUpUi() {
        setupViewPager();
    }

    private void setupViewPager() {

        ProductsInCategoryPagerAdapter adapter = new ProductsInCategoryPagerAdapter(
                ((ECartHomeActivity) context).getSupportFragmentManager());

        Set<String> keys = CenterRepository.getCenterRepository().getMapOfProductsInCategory().keySet();

        for (String string : keys) {
            adapter.addFrag(new ProductListFragment(string), string);
        }

        viewPager.setAdapter(adapter);
        tabs.setupWithViewPager(viewPager);
    }

    private class ProductsLoaderListener implements Response.Listener<FetchProductsResponse> {

        @Override
        public void onResponse(FetchProductsResponse response) {
            Map<String, ArrayList<Product>> mapOfProductsInCategory = CenterRepository.getCenterRepository().getMapOfProductsInCategory();
            mapOfProductsInCategory.clear();

            for (ProductBO productBO: response.getProducts()) {
                Product product = new Product();
                product.setUid(productBO.getUid());
                product.setItemName(productBO.getName());
                product.setDiscount(String.valueOf(productBO.getDiscount()));
                product.setImageURL(productBO.getImageUrl());
                product.setMRP(String.valueOf((long) productBO.getMrp()));
                product.setItemShortDesc(productBO.getDescription());
                product.setItemDetail(productBO.getLongDescription());
                product.setProductId(productBO.getUid());
                long sellMRP = (long)(productBO.getMrp() * (100 - productBO.getDiscount())/100);
                product.setSellMRP(String.valueOf(sellMRP));

                for (String tag: productBO.getTags()) {
                    if(!mapOfProductsInCategory.containsKey(tag)) {
                        mapOfProductsInCategory.put(tag, new ArrayList<Product>());
                    }
                    mapOfProductsInCategory.get(tag).add(product);
                }

                if (!mapOfProductsInCategory.containsKey(context.getString(R.string.ALL))) {
                    mapOfProductsInCategory.put(context.getString(R.string.ALL), new ArrayList<Product>());
                }
                mapOfProductsInCategory.get(context.getString(R.string.ALL)).add(product);
            }

            if (null != ((ECartHomeActivity) context).getProgressBar())
                ((ECartHomeActivity) context).getProgressBar().setVisibility(
                        View.GONE);
            setUpUi();

        }
    }

    private class ProductsLoaderErrorListener implements Response.ErrorListener {

        @Override
        public void onErrorResponse(VolleyError error) {
            Log.e(TAG, "onErrorResponse: ", error);

            Log.e(TAG, "onErrorResponse: ", error);
            Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_LONG).show();
            if (null != ((ECartHomeActivity) context).getProgressBar())
                ((ECartHomeActivity) context).getProgressBar().setVisibility(View.GONE);
        }
    }
}
