package store.bluescooter.retailapp.domain.api;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.tarkshala.dukan.business.model.FetchCategoriesResponse;
import com.tarkshala.dukan.business.model.bo.CategoryBO;
import com.tarkshala.volley.dukan.FetchRequest;

import java.util.ArrayList;

import store.bluescooter.retailapp.BlueScooterApplication;
import store.bluescooter.retailapp.R;
import store.bluescooter.retailapp.exceptions.NetworkRecoverableException;
import store.bluescooter.retailapp.model.CenterRepository;
import store.bluescooter.retailapp.model.entities.ProductCategoryModel;
import store.bluescooter.retailapp.util.AppConstants;
import store.bluescooter.retailapp.util.Utils;
import store.bluescooter.retailapp.view.activities.ECartHomeActivity;
import store.bluescooter.retailapp.view.adapter.CategoryListAdapter;
import store.bluescooter.retailapp.view.fragment.ProductOverviewFragment;

import static store.bluescooter.retailapp.BlueScooterApplication.TAG;

public class ProductCategoryLoader {

    private Context context;
    private RecyclerView recyclerView;
    private static final String ENTITY_NAME = "Category";
    private static final int VOLLEY_TIMEOUT = 30000;

    public ProductCategoryLoader(Context context, RecyclerView recyclerView) {
        this.context = context;
        this.recyclerView = recyclerView;
    }

    public Void load(String... params) {

        Log.i(TAG, "load: " + ENTITY_NAME);

        // do whatever is needed before starting task
        if (null != ((ECartHomeActivity) context).getProgressBar())
            ((ECartHomeActivity) context).getProgressBar().setVisibility(View.VISIBLE);

        Log.i(TAG, "doInBackground: " + ENTITY_NAME);
        FetchRequest<FetchCategoriesResponse> fetchRequest = new FetchRequest<FetchCategoriesResponse>(
                AppConstants.FETCH_CATEGORIES_URL,
                new ProductCategoryLoaderListener(),
                new ProductCategoryLoaderErrorListener(),
                FetchCategoriesResponse.class,
                true);
        fetchRequest.setRetryPolicy(new DefaultRetryPolicy(VOLLEY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        BlueScooterApplication.getInstance().getNetworkQueue().add(fetchRequest);
        return null;
    }

    private class ProductCategoryLoaderListener implements Response.Listener<FetchCategoriesResponse> {

        @Override
        public void onResponse(FetchCategoriesResponse response) {
            ArrayList<ProductCategoryModel> listOfCategory = CenterRepository.getCenterRepository().getListOfCategory();
            listOfCategory.clear();

            for (CategoryBO categoryBO : response.getCategories()) {
                String name = categoryBO.getName();
                String description = categoryBO.getDescription();
                String url = categoryBO.getImageUrl();
                listOfCategory.add(new ProductCategoryModel(name,description, "", url, categoryBO.getUid()));
            }

            if (null != ((ECartHomeActivity) context).getProgressBar())
                ((ECartHomeActivity) context).getProgressBar().setVisibility(View.GONE);

            if (recyclerView != null) {
                final CategoryListAdapter simpleRecyclerAdapter = new CategoryListAdapter(context);

                recyclerView.setAdapter(simpleRecyclerAdapter);

                simpleRecyclerAdapter
                        .SetOnItemClickListener(new CategoryListAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                CenterRepository.getCenterRepository().
                                        setCurrentSelectedCategory(
                                                simpleRecyclerAdapter
                                                        .getCategoryList()
                                                        .get(position));
                                ProductOverviewFragment productOverviewFragment = new ProductOverviewFragment();
                                String uid = simpleRecyclerAdapter.getCategoryList().get(position).getUid();
                                Utils.switchFragmentWithAnimation(
                                        R.id.frag_container,
                                        productOverviewFragment,
                                        ((ECartHomeActivity) context), Utils.PRODUCT_OVERVIEW_FRAGMENT_TAG,
                                        Utils.AnimationType.SLIDE_LEFT);
                            }
                        });
                simpleRecyclerAdapter.notifyDataSetChanged();
            }
        }
    }

    private class ProductCategoryLoaderErrorListener implements Response.ErrorListener {

        @Override
        public void onErrorResponse(VolleyError error) {
            Log.e(TAG, "onErrorResponse: ", error);
            Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_LONG).show();
            if (null != ((ECartHomeActivity) context).getProgressBar())
                ((ECartHomeActivity) context).getProgressBar().setVisibility(View.GONE);
        }
    }
}
