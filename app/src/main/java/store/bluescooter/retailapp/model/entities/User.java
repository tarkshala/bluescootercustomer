package store.bluescooter.retailapp.model.entities;

import com.google.android.gms.maps.model.LatLng;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User {

    private String name;
    private String email;
    private Address address;


    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public class Address {
        private String houseNumber;
        private String floorNumber;
        private String buildingName;
        private String societyName;

        private String blockNumber;
        private String sector;
        private String road;

        private LatLng location;
    }
}
