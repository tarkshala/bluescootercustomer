package com.tarkshala.volley.dukan;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;

import static android.content.ContentValues.TAG;

/**
 * This is abstraction of GET requests,
 * Payload(request body) is not allowed.
 *
 * @param <R> type of response
 */
public class FetchRequest<R> extends JsonRequest {


    private final static long CACHE_HIT_BUT_REFRESHED = 3 * 60 * 1000;
    private final static long CACHE_EXPIRED = 24 * 60 * 60 * 1000;

    private Type responseType;
    private boolean cacheTheResponse = false;

    public FetchRequest(String url, Response.Listener listener,
                        @Nullable Response.ErrorListener errorListener, Type responseType) {
        super(Method.GET, url, null, listener, errorListener);
        this.responseType = responseType;
    }

    public FetchRequest(String url, Response.Listener listener,
                        @Nullable Response.ErrorListener errorListener, Type responseType,
                        boolean cacheTheResponse) {
        super(Method.GET, url, null, listener, errorListener);
        this.responseType = responseType;
        this.cacheTheResponse = cacheTheResponse;
    }

    @Override
    protected Response<R> parseNetworkResponse(NetworkResponse response) {

        if (cacheTheResponse){
            return createResponseWithCacheEntry(response);
        }

        String responsePayload = null;
        try {
            responsePayload = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "parseNetworkResponse: " + responsePayload);
        R deserializedResponse = new Gson().fromJson(responsePayload, responseType);
        return Response.success(deserializedResponse, HttpHeaderParser.parseCacheHeaders(response));
    }

    private Response<R> createResponseWithCacheEntry(NetworkResponse response) {
        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
        if (cacheEntry == null) {
            cacheEntry = new Cache.Entry();
        }
        long now = System.currentTimeMillis();
        final long softExpire = now + CACHE_HIT_BUT_REFRESHED;
        final long ttl = now + CACHE_EXPIRED;

        cacheEntry.data = response.data;
        cacheEntry.softTtl = softExpire;
        cacheEntry.ttl = ttl;

        String headerValue;
        headerValue = response.headers.get("Date");
        if (headerValue != null) {
            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
        }
        headerValue = response.headers.get("Last-Modified");

        if (headerValue != null) {
            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
        }
        cacheEntry.responseHeaders = response.headers;
        try {
            final String responsePayload = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));

            R deserializedResponse = new Gson().fromJson(responsePayload, responseType);
            return Response.success(deserializedResponse, cacheEntry);
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    public String getCacheKey() {
        // TODO: 28/09/18 Check if we need to use url as part of cache key
        return getUrl();
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return 0;
    }
}
