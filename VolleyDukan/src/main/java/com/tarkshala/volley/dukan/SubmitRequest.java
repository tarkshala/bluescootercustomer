/*
 * Copyright (c) 2018. http://hiteshsahu.com- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * If you use or distribute this project then you MUST ADD A COPY OF LICENCE
 * along with the project.
 *  Written by Hitesh Sahu <hiteshkrsahu@Gmail.com>, 2017.
 */

package com.tarkshala.volley.dukan;

import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.UnsupportedEncodingException;

public class SubmitRequest<T> extends JsonRequest {

    public SubmitRequest(String url, T payload, Response.ErrorListener errorListener) {

        super(Method.POST, url,
                new GsonBuilder().setDateFormat("dd-MM-yyyy HH:mm:ss").create().toJson(payload),
                DoNothingListener.getInstance(), errorListener);
    }

    public SubmitRequest(String url, T payload, Response.Listener listener, Response.ErrorListener errorListener) {
        super(Method.POST, url,
                new GsonBuilder().setDateFormat("dd-MM-yyyy HH:mm:ss").create().toJson(payload),
                listener, errorListener);
    }

    @Override
    protected Response parseNetworkResponse(NetworkResponse response) {
        String responsePayload = null;
        try {
            responsePayload = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Response.success(responsePayload, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return 0;
    }

    /**
     * Do nothing listener, It will act as termination for the responses if the server replies.
     */
    private static class DoNothingListener implements Response.Listener {

        private static final DoNothingListener instance = new DoNothingListener();

        public static DoNothingListener getInstance() {
            return instance;
        }

        @Override
        public void onResponse(Object response) {
            Log.i("SubmitRequest", response.toString());
            // do nothing
        }
    }
}
